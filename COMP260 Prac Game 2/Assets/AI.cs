﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class AI : MonoBehaviour {

    private Rigidbody rb;
    private int minDis = -2;
    public Transform Puck;
    public float speed;
    public float thrust;

	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody>();
        rb.useGravity = false;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void FixedUpdate()
    {
        transform.LookAt(Puck);
        if(Vector3.Distance(transform.position, Puck.position) >= minDis)
        {
            rb.velocity = transform.forward * thrust;
        }
    }
}
