﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class Goal : MonoBehaviour {

    public AudioClip scoreClip;
    private AudioSource audio;
    public int goal; // which goal it enters

	// Use this for initialization
	void Start () {
        audio = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider collider)
    {
        // play score sound
        audio.PlayOneShot(scoreClip);

        // tell the scorekeeper
        Scorekeeper.Instance.OnScoreGoal(goal);

        // reset the puck to its starting position
        PuckControl puck = collider.gameObject.GetComponent<PuckControl>();
        puck.ResetPosition();
    }
}
