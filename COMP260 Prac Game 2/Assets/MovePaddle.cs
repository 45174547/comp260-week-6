﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class MovePaddle : MonoBehaviour {

    public float speed = 20f;
    public float force = 10f;
    private Rigidbody rigidbody;

	// Use this for initialization
	void Start () {
        rigidbody = GetComponent<Rigidbody>();
        rigidbody.useGravity = false;
	}
	
	// Update is called once per frame
	void Update () {
        Debug.Log("Time = " + Time.time);
	}

    private void FixedUpdate()
    {
        Debug.Log("Fixed Time = " + Time.fixedTime);
        Vector3 pos = GetMousePosition();
        Vector3 dir = pos - rigidbody.position;
        dir = dir.normalized;
        Vector3 vel = dir.normalized * speed;

        // check is this speed going to overshoot the target
        float move = speed * Time.fixedDeltaTime;
        float distToTarget = dir.magnitude;
        if(move > distToTarget)
        {
            // scale the velocity down appropriately
            vel = vel * distToTarget / move;
        }

        rigidbody.velocity = vel;
        rigidbody.AddForce(dir * force);
    }

    private Vector3 GetMousePosition()
    {
        // create a ray from the camera
        // passing through the mouse position
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        // find out where the ray intersects the XZ plane
        Plane plane = new Plane(Vector3.up, Vector3.zero);
        float distance = 0;
        plane.Raycast(ray, out distance);
        return ray.GetPoint(distance);
    }

    // draw the mouse ray
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Vector3 pos = GetMousePosition();
        Gizmos.DrawLine(Camera.main.transform.position, pos);
    }
}
