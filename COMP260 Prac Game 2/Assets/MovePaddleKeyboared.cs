﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovePaddleKeyboared : MonoBehaviour {

    Rigidbody rigidbody;
    float speed = 13f;
 
	// Use this for initialization
	void Start () {
        rigidbody = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        Vector3 fVelocity = new Vector3(horizontal, 0, vertical) * speed;
        rigidbody.velocity = fVelocity;
	}
}
