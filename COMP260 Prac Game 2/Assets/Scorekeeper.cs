﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Scorekeeper : MonoBehaviour {

    static private Scorekeeper instance;
    public int pointsPerGoal = 1;
    private int[] score = new int[2];
    public Text[] scoreText;
    public Text Winner;

	// Use this for initialization
	void Start () {
		if(instance == null)
        {
            // save this instance
            instance = this;
        }
        else
        {
            // more than one instance exists
            Debug.LogError("More than one Scorekeeper exists in the scene.");
        }

        ResetScores();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ResetScores()
    {
        // reset the scores to zero
        for (int i = 0; i < score.Length; i++)
        {
            score[i] = 0;
            scoreText[i].text = "0";
        }
    }

    public void OnScoreGoal(int goal)
    {
        score[goal] += pointsPerGoal;
        scoreText[goal].text = score[goal].ToString();
        if(scoreText[goal].text == "10")
        {
            if(goal == 0)
            {
                Winner.text = "Red Player Wins";
                ResetScores();
            }
            else
            {
                Winner.text = "Blue Player Wins";
                ResetScores();
            }
        }
    }

    static public Scorekeeper Instance
    {
        get { return instance; }
    }
}
